#ifndef MAIN_FRAME_HPP
#define MAIN_FRAME_HPP

#include "AlgorithmsPanel.hpp"
#include "FilesPanel.hpp"
#include "ControlsPanel.hpp"
#include "StatusPanel.hpp"

class MainFrame : public wxFrame {
  private:
    AlgorithmsPanel* algo_panel;
    FilesPanel* files_panel;
    StatusPanel* status_panel;
    ControlsPanel* controls_panel;

  public:
    MainFrame();
    ~MainFrame() override = default;

    StatusPanel& get_status_panel();
};

#endif
