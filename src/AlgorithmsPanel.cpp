#include "AlgorithmsPanel.hpp"

#include "Identifier.hpp"

namespace {
std::array<wxString, 2> symmetric_choices  = {"AES", "3DES"};
std::array<wxString, 1> asymmetric_choices = {"RSA"};
std::array<wxString, 1> hash_choices       = {"SHA2"};

std::array<wxString, 3> aes_choices        = {"128", "192", "256"};
std::array<wxString, 1> triple_des_choices = {"168"};

std::array<wxString, 3> rsa_choices = {"1024", "2048", "3072"};

std::array<wxString, 3> sha_choices = {"256", "384", "512"};

std::array<wxString, 2> block_choices = {"CBC", "CFB"};
}

AlgorithmsPanel::AlgorithmsPanel(wxWindow* parent)
    : wxPanel(parent),
      symmetric_rb(new wxRadioBox(this,
                                  ID::symmetric_rb,
                                  "Symmetric",
                                  wxDefaultPosition,
                                  wxDefaultSize,
                                  symmetric_choices.size(),
                                  symmetric_choices.data(),
                                  0,
                                  wxRA_SPECIFY_ROWS)),
      asymmetric_rb(new wxRadioBox(this,
                                   wxID_ANY,
                                   "Asymmetric",
                                   wxDefaultPosition,
                                   wxDefaultSize,
                                   asymmetric_choices.size(),
                                   asymmetric_choices.data(),
                                   0,
                                   wxRA_SPECIFY_ROWS)),
      hash_rb(new wxRadioBox(this,
                             wxID_ANY,
                             "Hash",
                             wxDefaultPosition,
                             wxDefaultSize,
                             hash_choices.size(),
                             hash_choices.data(),
                             0,
                             wxRA_SPECIFY_ROWS)),
      symmetric_c(new wxChoice(this,
                               wxID_ANY,
                               wxDefaultPosition,
                               wxDefaultSize,
                               aes_choices.size(),
                               aes_choices.data())),
      asymmetric_c(new wxChoice(this,
                                wxID_ANY,
                                wxDefaultPosition,
                                wxDefaultSize,
                                rsa_choices.size(),
                                rsa_choices.data())),
      hash_c(new wxChoice(this,
                          wxID_ANY,
                          wxDefaultPosition,
                          wxDefaultSize,
                          sha_choices.size(),
                          sha_choices.data())),
      block_mode_c(new wxChoice(this,
                                wxID_ANY,
                                wxDefaultPosition,
                                wxDefaultSize,
                                block_choices.size(),
                                block_choices.data()))
{
    auto sizer = new wxFlexGridSizer(3, 3, 10, 10);
    sizer->Add(symmetric_rb);
    sizer->Add(asymmetric_rb);
    sizer->Add(hash_rb);

    sizer->Add(symmetric_c);
    sizer->Add(asymmetric_c);
    sizer->Add(hash_c);

    sizer->Add(block_mode_c);
    SetSizer(sizer);

    symmetric_c->Select(1);
    asymmetric_c->Select(1);
    hash_c->Select(0);
    block_mode_c->Select(0);
}

void AlgorithmsPanel::on_symmetric_rb_click(wxCommandEvent& /*event*/)
{
    int selection = symmetric_rb->GetSelection();
    symmetric_c->Clear();
    switch (selection) {
        case 0: // AES
            symmetric_c->Append(aes_choices.size(), aes_choices.data());
            break;
        case 1: // TripleDES
            symmetric_c->Append(triple_des_choices.size(),
                                triple_des_choices.data());
            break;
        default: throw std::logic_error("Invalid selection index.");
    }
    symmetric_c->Select(0);
}

wxBEGIN_EVENT_TABLE(AlgorithmsPanel, wxPanel) // clang-format off
    EVT_RADIOBOX(ID::symmetric_rb, AlgorithmsPanel::on_symmetric_rb_click)
wxEND_EVENT_TABLE() // clang-format on
