#ifndef CONTROLS_PANEL_HPP
#define CONTROLS_PANEL_HPP

#include "AlgorithmsPanel.hpp"
#include "FilesPanel.hpp"
#include "FileInput.hpp"
#include "StatusPanel.hpp"

#include "Algorithm.hpp"

#include <botan/bigint.h>
#include <json.hpp>
#include <wx/wx.h>

class ControlsPanel : public wxPanel {
  private:
    const AlgorithmsPanel& algo_panel;
    const FilesPanel& files_panel;
    StatusPanel& status_panel;

    wxButton* envelope_btn;
    wxButton* signature_btn;
    wxButton* seal_btn;

  public:
    ControlsPanel(wxWindow* parent,
                  const AlgorithmsPanel& algo_panel,
                  const FilesPanel& files_panel,
                  StatusPanel& status_panel);

  private:
    void on_envelope_btn_pressed(wxCommandEvent& event);
    void on_signature_btn_pressed(wxCommandEvent& event);
    void on_seal_btn_pressed(wxCommandEvent& event);

    static void create_envelope(const Envelope::CreateObject& c_object,
                                const std::string& file_name,
                                SymmetricAlgorithm sa,
                                SymmetricMode mode,
                                AsymmetricAlgorithm aa,
                                bool append);
    static void read_envelope(const Envelope::ReadObject& r_object,
                              const std::string& file_name,
                              SymmetricAlgorithm sa,
                              SymmetricMode mode,
                              AsymmetricAlgorithm aa,
                              bool append);
    static void create_signature(const Signature::CreateObject& c_object,
                                 const std::string& file_name,
                                     const std::string& hash_type,
                                 AsymmetricAlgorithm aa,
                                 bool append);
    static void verify_signature(const Signature::VerifyObject& v_object,
                                 const std::string& file_name,
                                 const std::string& hash_type,
                                 AsymmetricAlgorithm aa,
                                 bool append);

    wxDECLARE_EVENT_TABLE();
};

#endif
