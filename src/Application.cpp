#include "Application.hpp"

#include <json.hpp>

bool Application::OnInit()
{
    main_frame = new MainFrame();
    main_frame->Show();

    return true;
}

bool Application::OnExceptionInMainLoop()
{
    auto& status_panel = main_frame->get_status_panel();
    try {
        throw;
    } catch (const nlohmann::json::type_error& e) {
        status_panel.set_status("Input file format error!", false);
        std::cout << e.what() << std::endl;
    } catch (const std::runtime_error& e) {
        status_panel.set_status(e.what(), false);
        std::cout << e.what() << std::endl;
    } catch(std::exception& e) {
        std::cout << e.what() << std::endl;
    } catch (...) {
        std::cout << "Unknown exception. Now exiting..." << std::endl;
        return false; // Ugasi.
    }
    return true;
}

wxIMPLEMENT_APP(Application);
