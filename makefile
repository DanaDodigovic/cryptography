# PROJECT STRUCTURE:
#      ProjectRoot
#       |- src       # Source files. Can contain subdirectories.
#       |- include   # Header files. Can contain subdirectories.
#       |- obj       # Intermediate (object) files. Mirrors source tree.
#       |- res       # Project resources.
#       |- exe       # Executable. Built with 'make'.


# ------------------------- GENERAL PROJECT SETTINGS ---------------------------
EXECUTABLE = LAB2
CXX = g++

# Directory structure.
INCLUDE_DIR = include
SRC_DIR = src
OBJ_DIR = obj
# ------------------------------------------------------------------------------


# ------------------------------- COMPILER FLAGS -------------------------------
# Configuration flags.
DEBUG_FLAGS = -ggdb3 -O0
RELEASE_FLAGS = -O3 -march=native

# Preprocessor and compiler flags.
CPPFLAGS  = -Wall -Wextra -pedantic-errors -MMD -MP -I$(INCLUDE_DIR)
CPPFLAGS += -I/usr/include/botan-2
CPPFLAGS += -Ilib/include

# WxWidgets flags.
CPPFLAGS += -isystem/usr/lib/wx/include/gtk3-unicode-3.0
CPPFLAGS += -isystem/usr/include/wx-3.0
CPPFLAGS += -D_FILE_OFFSET_BITS=64 -DWXUSINGDLL -D__WXGTK__
CPPFLAGS += -pthread

CXXFLAGS = -std=c++17
# ------------------------------------------------------------------------------


# -------------------------------- LINKER FLAGS --------------------------------
# Linker flags and libraries to link against.
LDFLAGS =
LDLIBS = -lbotan-2
LDLIBS += `wx-config-gtk3 --libs`
# ------------------------------------------------------------------------------


# ------------------------------ HELPER COMMANDS -------------------------------
# Directory guard. Used to create directory if a file requires it.
DIRECTORY_GUARD = @mkdir -p $(@D)
# ------------------------------------------------------------------------------


# --------------------------- SOURCE AND OBJECT FILES --------------------------
# All source files.
SRC = $(shell find $(SRC_DIR) -name '*.cpp')

# All object files.
OBJ = $(SRC:$(SRC_DIR)/%=$(OBJ_DIR)/%.o)

# Make dependency files - with paths from the project root.
DEP = $(OBJ:.o=.d)
# ------------------------------------------------------------------------------


# --------------------------------- PHONY RULES --------------------------------
# Special words. Ignore files with those names.
.PHONY: clean debug release
# ------------------------------------------------------------------------------


# ------------------------------ BUILDING RECIPES ------------------------------
# Debug build.
debug: CPPFLAGS += $(DEBUG_FLAGS)
debug: $(EXECUTABLE)

# Release build.
release: CPPFLAGS += $(RELEASE_FLAGS)
release: $(EXECUTABLE)

# Linking.
$(EXECUTABLE): $(OBJ)
	$(CXX) $(LDFLAGS) $^ $(LDLIBS) -o $@

# Compiling source files.
$(OBJ_DIR)/%.cpp.o: $(SRC_DIR)/%.cpp
	$(DIRECTORY_GUARD)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@
# ------------------------------------------------------------------------------


# --------------------------------- CLEANING -----------------------------------
# Remove object directories and executable files.
clean:
	$(RM) $(OBJ_DIR) -r
	$(RM) $(EXECUTABLE)
# ------------------------------------------------------------------------------


# ------------------------------ COMPILE FLAGS ---------------------------------
compile-flags:
	@printf '%s\n' $(CPPFLAGS) $(CXXFLAGS) > compile_flags.txt
# ------------------------------------------------------------------------------


# ------------------------- FLOTAING RULE FOR BSPWM ----------------------------
floating-rule:
	bspc rule -a LAB2 state=floating
# ------------------------------------------------------------------------------


# ---------------------------------- OTHER -------------------------------------
# Make source file recompile if header file that it includes has changed. This
# requires -MD flag passed to the compiler.
-include $(DEP)
# ------------------------------------------------------------------------------
