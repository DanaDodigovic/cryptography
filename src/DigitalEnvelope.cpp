#include "DigitalEnvelope.hpp"

#include <botan/auto_rng.h>
#include <botan/hex.h>

DigitalEnvelope::DigitalEnvelope(Botan::secure_vector<uint8_t>&& encrypted_key,
                                 Botan::secure_vector<uint8_t>&& cyphertext)
    : encrypted_key(encrypted_key), cyphertext(cyphertext)
{}

DigitalEnvelope DigitalEnvelope::create(const std::string& encrypted_key,
                                        const std::string& cyphertext)
{
    auto enc_key = Botan::hex_decode_locked(encrypted_key);
    auto cyph    = Botan::hex_decode_locked(cyphertext);

    return {std::move(enc_key), std::move(cyph)};
}

DigitalEnvelope DigitalEnvelope::create(const std::string& key,
                                        const std::string& plaintext,
                                        const std::vector<uint8_t> iv,
                                        Botan::PK_Encryptor_EME& rsa_encryptor,
                                        SymmetricAlgorithm sa,
                                        SymmetricMode mode)
{
    // Pretvori simetricni kljuc u niz bajtova.
    auto symmetric_key = Botan::hex_decode(key); // TODO

    // Enkriptiraj simetricni kljuc sa RSA, sa procitanom velicinom N-a i e-om.
    Botan::AutoSeeded_RNG rng;

    const auto ct = rsa_encryptor.encrypt(symmetric_key, rng);
    Botan::secure_vector<uint8_t> sct(ct.begin(), ct.end());

    // Enkriptiraj poruku sa zeljenim simetricnim algoritmom i nacinom.
    auto encryptor = ::create(sa, mode, true);

    // Pretvori zeljeni tekst u niz bajtova.
    Botan::secure_vector<uint8_t> pt(plaintext.data(),
                                     plaintext.data() + plaintext.length());

    // Kriptiraj (i zapisi u pt).
    encryptor->set_key(symmetric_key);
    encryptor->start(iv);
    encryptor->finish(pt);

    return {std::move(sct), std::move(pt)};
}

Botan::secure_vector<uint8_t>
DigitalEnvelope::get_decrypted_key(Botan::PK_Decryptor_EME& rsa_decryptor) const
{
    return rsa_decryptor.decrypt(encrypted_key);
}

Botan::secure_vector<uint8_t>
DigitalEnvelope::get_decrypted_text(const std::string& key,
                                    const std::vector<uint8_t>& iv,
                                    SymmetricAlgorithm sa,
                                    SymmetricMode mode) const
{
    auto decryptor      = ::create(sa, mode, false);
    auto decrypted_text = cyphertext;

    decryptor->set_key(Botan::hex_decode(key));
    decryptor->start(iv);
    decryptor->finish(decrypted_text);

    return decrypted_text;
}

Botan::secure_vector<uint8_t> DigitalEnvelope::get_encrypted_key() const
{
    return encrypted_key;
}

Botan::secure_vector<uint8_t> DigitalEnvelope::get_cyphertext() const
{
    return cyphertext;
}
