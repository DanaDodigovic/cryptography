#ifndef DIGITAL_SIGNATURE_HPP
#define DIGITAL_SIGNATURE_HPP

#include <botan/pubkey.h>

class DigitalSignature {
  private:
    std::vector<uint8_t> signature;

  public:
    static DigitalSignature create(std::string plaintext,
                                   Botan::PK_Signer& signer);

    static bool verify(const std::string& plaintext,
                       const std::vector<uint8_t>& signature,
                       Botan::PK_Verifier& verifier);

    std::vector<uint8_t> get_signature() const;

  private:
    DigitalSignature(const std::vector<uint8_t>& signature);
};

#endif
