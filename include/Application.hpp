#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include "MainFrame.hpp"

class Application : public wxApp {
  private:
    MainFrame* main_frame = nullptr;

  public:
    bool OnInit() override;
    bool OnExceptionInMainLoop() override;
};

wxDECLARE_APP(Application);

#endif

