#ifndef FILES_PANEL_HPP
#define FILES_PANEL_HPP

#include <wx/wx.h>

class FilesPanel : public wxPanel {
  public:
    wxTextCtrl* input_file_box  = nullptr;
    wxTextCtrl* output_file_box = nullptr;

  public:
    FilesPanel(wxWindow* parent);
};

#endif
