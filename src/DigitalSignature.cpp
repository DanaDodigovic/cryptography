#include "DigitalSignature.hpp"

#include <botan/auto_rng.h>
#include <botan/pubkey.h>
#include <botan/rsa.h>
#include <botan/hex.h>

DigitalSignature DigitalSignature::create(std::string plaintext,
                                          Botan::PK_Signer& signer)
{
    Botan::AutoSeeded_RNG rng;

    Botan::secure_vector<uint8_t> pt(plaintext.data(),
                                     plaintext.data() + plaintext.length());

    return {signer.sign_message(pt, rng)};
}

bool DigitalSignature::verify(const std::string& plaintext,
                              const std::vector<uint8_t>& signature,
                              Botan::PK_Verifier& verifier)
{
    Botan::secure_vector<uint8_t> pt(plaintext.data(),
                                     plaintext.data() + plaintext.length());
    return verifier.verify_message(pt, signature);
}

DigitalSignature::DigitalSignature(const std::vector<uint8_t>& signature)
    : signature(signature)
{}

std::vector<uint8_t> DigitalSignature::get_signature() const
{
    return signature;
}
